from qlearning_agent import QLearning
from neural_agent import QNeural
from other_player import IA
import pickle

from torch.nn import MSELoss
loss = MSELoss()
ia = IA()

with open("./Trained Qtables and MCTS agent/QLearning_QL_vs_ia_100000_eps01_", "rb") as input:
    x_QT_ia = QLearning(epsilon = 0.1, var_explore= False, playing=True)
    x_QT_ia.set_tables(pickle.Unpickler(input).load())

with open("./Trained Qtables and MCTS agent/QLearning_ia_vs_QL_100000_eps01_", "rb") as input:
    o_QT_ia = QLearning(epsilon = 0.1, var_explore= False, playing=True)
    o_QT_ia.set_tables(pickle.Unpickler(input).load())

with open("./Trained Qtables and MCTS agent/QLearning_QL_vs_R_100000_eps01_", "rb") as input:
    x_QT = QLearning(epsilon = 0.1, var_explore= False, playing=True)
    x_QT.set_tables(pickle.Unpickler(input).load())

x_QN = QNeural(loss_function= loss, var_explore= False, epsilon= 0.1, playing=True)
x_QN.load("./neural_checkpoints/checkpoint_QN_vs_R_100000_eps01__1646184445_100000")

o_QN = QNeural(loss_function= loss, var_explore= False, epsilon= 0.1, playing=True)
o_QN.load("./neural_checkpoints/checkpoint_R_vs_QN_100000_eps01__1646212660_100000")

x_players = [x_QT, x_QN]
o_players =  [o_QT_ia, o_QN]