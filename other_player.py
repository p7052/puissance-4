# reference https://github.com/eserandour/Puissance_4
""" L'IA implémenté ici est l'ia 15 de la référence suscitée"""

import random
from player import AbstractPlayer
import numpy as np

NB_LIGNES = 6
ALIGNEMENT = 4 
NB_COLONNES = 7

########################################################################

def alignement(somme, nbPions, couleur):
    """Analyse la somme dont il est question dans alignements_pleins() ou alignements_troues() pour détermminer si des pions sont alignés"""
    pionsAlignes = False
    if (couleur == 1 and somme == nbPions) or (couleur == -1 and somme == -nbPions):
        pionsAlignes = True
    return pionsAlignes

########################################################################

def alignements_pleins(positions, nbPions, couleur):
    """Teste les alignements pleins d'un nombre de pions donné et les retourne sous forme de liste"""
    """
    4 pions alignés : 1111
    3 pions alignés : 111
    2 pions alignés : 11
    1 pion "aligné" : 1
    """
    listeAlignementsPleins = []
    # Vérification des alignements horizontaux
    for j in range(NB_LIGNES):
        for i in range(NB_COLONNES-nbPions+1):
            somme = 0
            for k in range(nbPions):
                somme += positions[NB_COLONNES*j+i+k]
            if alignement(somme, nbPions, couleur):
                listeAlignementsPleins += [i+1,j+1,"H"]
    # Vérification des alignements verticaux
    for j in range(NB_LIGNES-nbPions+1):
        for i in range(NB_COLONNES):
            somme = 0
            for k in range(nbPions):
                somme += positions[NB_COLONNES*j+i+k*NB_COLONNES]
            if alignement(somme, nbPions, couleur):
                listeAlignementsPleins += [i+1,j+1,"V"]
    # Vérification des diagonales montantes
    for j in range(NB_LIGNES-nbPions+1):
        for i in range(NB_COLONNES-nbPions+1):
            somme = 0
            for k in range(nbPions):
                somme += positions[NB_COLONNES*j+i+k*NB_COLONNES+k]
            if alignement(somme, nbPions, couleur):
                listeAlignementsPleins += [i+1,j+1,"DM"]
    # Vérification des diagonales descendantes
    for j in range(nbPions-1, NB_LIGNES):
        for i in range(NB_COLONNES-nbPions+1):
            somme = 0
            for k in range(nbPions):
                somme += positions[NB_COLONNES*j+i-k*NB_COLONNES+k]
            if alignement(somme, nbPions, couleur):
                listeAlignementsPleins += [i+1,j+1,"DD"]
    if listeAlignementsPleins != []:
        listeAlignementsPleins = [nbPions] + listeAlignementsPleins
    return listeAlignementsPleins

########################################################################

def alignements_troues(positions, nbPions, couleur):
    """Teste les alignements troués d'un nombre de pions donné et les retourne sous forme de liste"""
    """
    3 pions alignés : 1110 / 1101 / 1011 / 0111
    2 pions alignés : 110 / 101 / 011
    1 pion "aligné" : 10 / 01
    """
    listeAlignementsTroues = []
    # Vérification des alignements horizontaux
    for j in range(NB_LIGNES):
        for i in range(NB_COLONNES-nbPions):
            somme = 0
            for k in range(nbPions+1):
                somme += positions[NB_COLONNES*j+i+k]
            if alignement(somme, nbPions, couleur):
                listeAlignementsTroues += [i+1,j+1,"H"]
    # Vérification des alignements verticaux
    for j in range(NB_LIGNES-nbPions):
        for i in range(NB_COLONNES):
            somme = 0
            for k in range(nbPions+1):
                somme += positions[NB_COLONNES*j+i+k*NB_COLONNES]
            if alignement(somme, nbPions, couleur):
                listeAlignementsTroues += [i+1,j+1,"V"]
    # Vérification des diagonales montantes
    for j in range(NB_LIGNES-nbPions):
        for i in range(NB_COLONNES-nbPions):
            somme = 0
            for k in range(nbPions+1):
                somme += positions[NB_COLONNES*j+i+k*NB_COLONNES+k]
            if alignement(somme, nbPions, couleur):
                listeAlignementsTroues += [i+1,j+1,"DM"]
    # Vérification des diagonales descendantes
    for j in range(nbPions, NB_LIGNES):
        for i in range(NB_COLONNES-nbPions):
            somme = 0
            for k in range(nbPions+1):
                somme += positions[NB_COLONNES*j+i-k*NB_COLONNES+k]
            if alignement(somme, nbPions, couleur):
                listeAlignementsTroues += [i+1,j+1,"DD"]
    if listeAlignementsTroues != []:
        listeAlignementsTroues = [nbPions] + listeAlignementsTroues
    return listeAlignementsTroues

########################################################################

def inverse(couleur):
    """ Inverse les couleurs"""
    if couleur == 1:
        couleur = 2
    elif couleur == 2:
        couleur = 1
    return couleur

def priorite_trouee(positions, nbPions, couleur):
    """Retourne une colonne où jouer"""
    listeAlignementsTroues = alignements_troues(positions, nbPions-1, couleur)
    return positions_potentielles(positions, listeAlignementsTroues)

def positions_potentielles(positions, listeAlignementsTroues):
    """Retourne une colonne où jouer à partir de l'ensemble des positions potentielles"""
    positionsPotentielles = []
    if listeAlignementsTroues != []:
        nbPions = listeAlignementsTroues[0]
        for i in range(0, len(listeAlignementsTroues) // 3):
            c = listeAlignementsTroues[1 + 3*i] # Colonne
            l = listeAlignementsTroues[2 + 3*i] # Ligne
            d = listeAlignementsTroues[3 + 3*i] # Direction
            if d == "H": # Horizontal
                for j in range(nbPions + 1):
                    if position_potentielle(positions, c + j, l):
                        positionsPotentielles += [position(c + j, l)]
            if d == "V": # Vertical
                if position_potentielle(positions, c, l + nbPions):
                    positionsPotentielles += [position(c, l + nbPions)]
            if d == "DM": # Diagonale Montante
                for j in range(nbPions + 1):
                    if position_potentielle(positions, c + j, l + j):
                        positionsPotentielles += [position(c + j, l + j)]
            if d == "DD": # Diagonale Descendante
                for j in range(nbPions + 1):
                    if position_potentielle(positions, c + j, l - j):
                        positionsPotentielles += [position(c + j, l - j)]
    colonne = -1
    if len(positionsPotentielles) > 0:
        colonne = colonne_extraite(meilleure_position(positionsPotentielles))
    return colonne

########################################################################

def position_potentielle(positions, colonne, ligne):
    """Teste si une position est possible (case vide et support pour soutenir le pion)"""
    test = False
    if colonne >= 1 and colonne <= NB_COLONNES and ligne >= 1 and ligne <= NB_LIGNES:
        if positions[position(colonne, ligne)] == 0:  # Position libre
            test = True
            if ligne > 1:
                if positions[position(colonne, ligne - 1)] == 0:  # Ligne support inexistante
                    test = False
    return test

########################################################################

def position(colonne, ligne):
    """Convertit une position sous forme de coordonnées dans la grille en une position sous forme d'indice dans la liste positions[]"""
    """
    Pour une grille (7x6) :
    La grille est codée sous la forme d'une liste : [0,1,2,3,...,41]
    
     1  2  3  4  5  6  7  <= colonnes
    
    35 36 37 38 39 40 41  <= ligne 6
    28 29 30 31 32 33 34  <= ligne 5
    21 22 23 24 25 26 27  <= ligne 4
    14 15 16 17 18 19 20  <= ligne 3
     7  8  9 10 11 12 13  <= ligne 2
     0  1  2  3  4  5  6  <= ligne 1
    
    Pour la conversion, par exemple :
    (colonne = 3 ; ligne = 4) renvoie l'indice 23 dans la liste positions
    """
    position = (colonne-1) + (ligne-1)*NB_COLONNES
    return position

########################################################################

def colonne_extraite(position):
    """Déduit d'une position dans la grille la colonne correspondante"""
    colonne = position % NB_COLONNES + 1
    return colonne

########################################################################

def meilleure_position(positionsPotentielles):
    """Détermine la meilleure position en s'appuyant sur le poids des cases"""
    # Calcule le poids des cases
    poidsCases = poids_cases()
    # Détermine le poids des positions potentielles
    poidsPositionsPotentielles = []
    for i in range(len(positionsPotentielles)):
        poidsPositionsPotentielles += [poidsCases[positionsPotentielles[i]]]
    # Détermine les indices du poids maximum dans la liste ci-dessus
    indicesPoidsMaximum = liste_indices_maximum(poidsPositionsPotentielles)
    # Extrait les meilleures positions potentielles (celles qui ont un poids maximum)
    meilleuresPositionsPotentielles = []
    for i in range(len(indicesPoidsMaximum)):
        meilleuresPositionsPotentielles += [positionsPotentielles[indicesPoidsMaximum[i]]]
    # Si plusieurs positions sont possibles (même poids), on tire au hasard une position
    return random.choice(meilleuresPositionsPotentielles)

########################################################################

def liste_indices_maximum(liste):
    """Renvoie les indices des maximums d'une liste"""
    maxi = max(liste)
    indices = []
    for i in range(len(liste)):
        if liste[i] == maxi:
            indices += [i]
    return indices

########################################################################

def poids_cases():
    """Calcule le poids des cases en fonction de la dimension de la grille et du nombre de pions à aligner pour gagner"""
    """[3,4,5,7,5,4,3,4,6,8,10,8,6,4,5,8,11,13,11,8,5,5,8,11,13,11,8,5,4,6,8,10,8,6,4,3,4,5,7,5,4,3] pour une grille 7x6 avec 4 pions à aligner"""
    poids = [0] * NB_COLONNES*NB_LIGNES
    # Sur les horizontales
    for j in range(NB_LIGNES):
        for i in range(NB_COLONNES-ALIGNEMENT+1):
            for k in range(ALIGNEMENT):
                poids[NB_COLONNES*j+i+k] += 1
    # Sur les verticales
    for j in range(NB_LIGNES-ALIGNEMENT+1):
        for i in range(NB_COLONNES):
            for k in range(ALIGNEMENT):
                poids[NB_COLONNES*j+i+k*NB_COLONNES] += 1
    # Sur les diagonales montantes
    for j in range(NB_LIGNES-ALIGNEMENT+1):
        for i in range(NB_COLONNES-ALIGNEMENT+1):
            for k in range(ALIGNEMENT):
                poids[NB_COLONNES*j+i+k*NB_COLONNES+k] += 1
    # Sur les diagonales descendantes
    for j in range(ALIGNEMENT-1, NB_LIGNES):
        for i in range(NB_COLONNES-ALIGNEMENT+1):
            for k in range(ALIGNEMENT):
                poids[NB_COLONNES*j+i-k*NB_COLONNES+k] += 1
    return poids

########################################################################

def jouer_ordi_poids_colonnes(positions):
    """L'ordinateur joue en ne tenant compte que du poids des cases de la grille potentiellement victorieuses"""
    poidsCases = poids_cases()
    poidsColonnes = [0] * NB_COLONNES
    for colonne in range(1, NB_COLONNES + 1):
        if not colonne_pleine(positions, colonne):
            position = colonne - 1
            while positions[position]:
                position += NB_COLONNES
            poidsColonnes[colonne - 1] += poidsCases[position]
        else:
            poidsColonnes[colonne - 1] += 0
    indicesPoidsMaximum = liste_indices_maximum(poidsColonnes)
    # Si plusieurs colonnes sont possibles (même poids), on tire au hasard une colonne
    colonne = 1 + random.choice(indicesPoidsMaximum)
    return colonne

########################################################################

def jouer_ordi_poids_cases(positions, couleur):
    """L'ordinateur joue en ne tenant compte que du poids des cases de la grille potentiellement victorieuses"""
    
    colonne = jouer_ordi_poids_colonnes(positions)
    return jouer(positions, couleur, colonne)

########################################################################

def jouer(positions, couleur, colonne):
    """Moteur du jeu"""
    if not colonne_pleine(positions, colonne):
        # On remplit la liste des positions
        position = colonne - 1
        ligneSupport = 0
        while positions[position]:
            ligneSupport += 1
            position += NB_COLONNES
        if couleur == 1:
            valeur = 1
        elif couleur == 2:
            valeur = -1            
        positions[position] = valeur
    return positions

########################################################################

def colonne_pleine(positions, colonne):
    """Teste si la colonne indiquée est pleine"""
    plein = True
    position = NB_COLONNES*(NB_LIGNES-1)+colonne-1
    if positions[position] == 0:
        plein = False
    return plein


class IA(AbstractPlayer):
    """docstring for Human"""

    def __init__(self):
        super().__init__("IA")

    def get_colonne(self, board):

        if not board.is_game_over():
            
            couleur = self.turn
            positions = board.cells_2d()

            for i in range(6):
                positions[i, :] = list(reversed(positions[i, :]))
            
            positions = list(reversed(list(positions.reshape(1,-1)[0])))
            
            colA4PH = priorite_trouee(positions, 4, couleur)
            colA3PH = priorite_trouee(positions, 3, couleur)
            colA2PH = priorite_trouee(positions, 2, couleur)
            couleurAdversaire = inverse(couleur)
            colB4PH = priorite_trouee(positions, 4, couleurAdversaire)
            colB3PH = priorite_trouee(positions, 3, couleurAdversaire)
            colB2PH = priorite_trouee(positions, 2, couleurAdversaire)
            if colA4PH != -1:
                return colA4PH -1
            elif colB4PH != -1:
                return colB4PH -1
            elif colA3PH != -1:
                return colA3PH -1
            elif colB3PH != -1:
                return colB3PH - 1
            elif colB2PH != -1:
                return colB2PH -1
            elif colA2PH != -1:
                return colA2PH - 1
            else:
                return jouer_ordi_poids_colonnes(positions) -1

    def jouer_ia(self, board):
        """IA5 joue"""
        if not board.is_game_over():
            
            couleur = self.turn
            positions = board.cells_2d()

            for i in range(6):
                positions[i, :] = list(reversed(positions[i, :]))
            
            positions = list(reversed(list(positions.reshape(1,-1)[0])))
            
            colonne = self.get_colonne(board) + 1
            positions = jouer(positions, couleur, colonne)

            positions = list(reversed(positions))

            positions = np.array(positions).reshape(6,7)

            for i in range(6):
                positions[i, :] = list(reversed(positions[i, :]))
            
            return positions
    
    def get_best_move(self, board):

        raise NotImplementedError
