import numpy as np

class Transform:
    """
        Class to apply some transformations on board
    """

    def __init__(self, operations):
        self.operations = operations

    def execute(self, array):
        for operation in self.operations:
            array = operation(array)

        return array

TRANSFORMS = [Transform([]),
              Transform([np.fliplr])]


class Cache(object):
    """
        It's the memory for player training
    """

    def __init__(self):
        super(Cache, self).__init__()
        self.boards = {}

    def set(self, board, value):
        for transform in TRANSFORMS:
            cells_2d = transform.execute(board.cells_2d())
            self.boards[cells_2d.tobytes()] = value

    def get(self, board):
        
        result = self.boards.get(board.cells_2d().tobytes(), None)

        if result is None:
            return None, False

        return result, True
    
    def reset(self):
        self.cache = {}