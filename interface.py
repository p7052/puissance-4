import random
from tkinter import *
from params import Parametres
from board import Board
from board_graphics import Carte

import numpy as np
import time
from computer_players import o_players, x_players



if __name__ == '__main__':

    X_players_ = x_players[:]
    O_players_ = o_players[:]

    wcarte = Tk()
    wcarte.config(bg='red') #The background color of the window corresponds to the player
    wcarte.title('Puissance 4')
    wcarte.resizable(width=FALSE, height=FALSE)

    params=Parametres()

    lb_joueur=Label(wcarte,bg="white") #Label indicating player who plays

    def echange_valeur(x,y): #Update the value of a desired cell
        global valeur_actuelle

              
        valeur_actuelle=matrice[x][y] #Gets the current state of the box
        
        if valeur_actuelle==0:
            if(joueur==0):
                nouvelle_valeur=1
            else:
                nouvelle_valeur=2
            matrice[x][y]=nouvelle_valeur
            
        carte.change_parcelle(x,y,matrice[x][y])

        verifier(x,5-jetons[x]) #Checks if a player has won

    def change_joueur():
        global joueur, valeur_actuelle, nbClic
        
        if valeur_actuelle == 0:
            joueur=1-joueur
            lb_joueur.config(text="-   Joueur " + str(joueur + 1) + "   -")
            
            if joueur == 0:
                wcarte.config(bg="red")
            else:
                wcarte.config(bg="yellow")


    def action(evt): #Called when there is a left click on the grid
        global nbClic, nbCase, board, computer

        if nbClic != nbCase:
            x=carte.get_x(evt) #Gets the position of the click on the x axis

            if jetons[x] < 6 : #Cheks if it is a valid move
                board.execute_turn(x)
                echange_valeur(x,5-jetons[x])
                jetons[x] += 1
                change_joueur()
                nbClic += 1
            
            if nbClic != nbCase:
                if computer.name == "IA":
                    x = computer.get_colonne(board)
                else:
                    x = computer.get_best_move(board)

                if x == None:
                    x = board.get_random_valid_move()
                    board.execute_turn(x)

                    echange_valeur(x,5-jetons[x])
                    jetons[x] += 1
                    change_joueur()
                    nbClic += 1
                else:
                    if jetons[x] < 6 :
                        if computer.name == "IA":
                            board.cells = computer.jouer_ia(board)
                        else:
                            board.execute_turn(x)
                        
                        echange_valeur(x,5-jetons[x])
                        jetons[x] += 1
                        change_joueur()
                        nbClic += 1

        else:
            fin()

    def verifier(x,y): #Checks if a player wins
        joueur = matrice[x][y]
        compte = 1 

        #Right
        decalage = 1
        while(x + decalage < 7 and matrice[x + decalage][y] == joueur):
            compte += 1
            decalage += 1

        #Left
        decalage = 1
        while(x - decalage >= 0 and matrice[x - decalage][y] == joueur):
            compte += 1
            decalage += 1

        #Reset
        if(compte >= 4):
            fin()
            return
        compte = 1

        #Down
        decalage = 1
        while(y + decalage < 6 and matrice[x][y + decalage] == joueur):
            compte += 1
            decalage += 1

        #Reset
        if(compte >= 4):
            fin()
            return
        compte = 1

        # Down right 
        decalage = 1
        while(y + decalage < 6 and x + decalage < 7 and matrice[x + decalage][y + decalage] == joueur):
            compte += 1
            decalage += 1

        #Up left
        decalage = 1
        while(y - decalage >= 0 and x - decalage >= 0 and matrice[x - decalage][y - decalage] == joueur):
            compte += 1
            decalage += 1

        #Reset
        if(compte >= 4):
            fin()
            return
        compte = 1

        #Down left
        decalage = 1
        while(y + decalage < 6 and x - decalage >= 0 and matrice[x - decalage][y + decalage] == joueur):
            compte += 1
            decalage += 1

        #Up Right
        decalage = 1
        while(y - decalage >= 0 and x + decalage < 7 and matrice[x + decalage][y - decalage] == joueur):
            compte += 1
            decalage += 1

        #Reset
        if(compte >= 4):
            fin()
            return

    def fin(): #Displays the end of game menu
        global affMenu, menu, joueur, human_turn, board

        if affMenu == False:
            affMenu = True
            
            menu = Tk()
            menu.title('Menu')
            menu.resizable(width=FALSE, height=FALSE)
            menu.geometry("400x100")
            
            if board.get_game_result() == 0:
                label_gagne = Label(menu,bg='white', text="Match nul !!!")
                label_gagne.pack()
                menu.config(bg="green")
                label_gagne.config(bg="green")
            else:
                if human_turn == joueur:
                    label_gagne = Label(menu,bg='white', text="Félicitations!!! Vous avez gagné.")
                else:
                    label_gagne = Label(menu,bg='white', text="L'ordinateur a gagné.")
                
                label_gagne.pack()

            #The box takes the color of the winning player
            if joueur == 0:
                menu.config(bg="red")
                label_gagne.config(bg="red")
            else:
                menu.config(bg="yellow")
                label_gagne.config(bg="yellow")
            
            button_reco = Button(menu,bg='beige', text="Recommencer", command=recommencer)
            button_reco.pack()
            
            button_quit = Button(menu,bg='beige', text="Quitter", command=quitter)
            button_quit.pack()
            
            menu.mainloop()

    def recommencer():
        menu.destroy()
        init()

    def quitter():
        menu.destroy()
        wcarte.destroy()  

    def init(): #Initialize the grid
        global joueur,carte,matrice,nbCase,nbClic,affMenu,jetons, board, computer, human_turn

        board = Board()
        nbClic = 0
        affMenu = False
        jetons = [0,0,0,0,0,0,0,0]

        for widget in wcarte.winfo_children():
            widget.pack_forget()

        matrice=[[0 for y in range(6)]
                 for x in range(7)]
        
        nbCase = 6 * 7
        
        joueur=0
        lb_joueur.config(text="-   Joueur 1   -")
        lb_joueur.pack()
        wcarte.config(bg='red')

        carte=Carte(wcarte,params,matrice)

        carte.focus_set()
        
        if np.random.uniform() > 0.5:
            
            human_turn = 1
            computer = random.choice(X_players_)
            computer.set_turn(1)
            if computer.name == "IA":
                x = computer.get_colonne(board)
            else:
                x = computer.get_best_move(board)

            if x == None:
                x = board.get_random_valid_move()
                board.execute_turn(x)

                echange_valeur(x,5-jetons[x])
                jetons[x] += 1
                change_joueur()
                nbClic += 1
            else:
                if jetons[x] < 6 :
                    if computer.name == "IA":
                        board.cells = computer.jouer_ia(board)
                    else:
                        board.execute_turn(x)
                    
                    echange_valeur(x,5-jetons[x])
                    jetons[x] += 1
                    change_joueur()
                    nbClic += 1
        
        else:
            human_turn = 0
            computer = random.choice(O_players_)
            computer.set_turn(2)
        carte.bind("<Button-1>",action)

    init()

    wcarte.mainloop()