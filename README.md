# Reinforcement learning for Connect 4 known as "Puissance 4" in french

## Project's Description

This project implements the game "Connect 4" as an application of Reinforcement Learning. We implement and train several agents using Tabular Q-Learning and Neural Network with the epsilon greedy method as action selection technique. We also propose some agents using the Minimax and Monte Carlo Tree Search (MCTS) algorithm. We propose at the end a graphical interface that allows a human user to play against some trained agents.

## Project's content

This is a brief description of the different files and folders contained in this project.

### Folders

This project contains three folders `learning_records`, `neural checkpoint` and `Trained Qtables and MCTS agent`.

+ `learning_records` contains `.csv` files with the performance records of the agents during their training.
+ `neural checkpoint` contains the neural agent checkpoints which can be loaded and used for playing.
+ `Trained Qtables and MCTS agent` contains the memory or cache of the trained agent using Tabular Q-Learning and MCTS.

### Files

The following files are related to the implementation of agents or players.

+ `player.py` contains abstract class from which any agent or player inherits
+ `minimax_player.py` implements an agent using minimax algorithm to choose action
+ `random_player.py` implements an agent which takes random decision
+ `qlearning_player.py` implements an agent using Tabular Q-Learning or Tabular Double Q-Learning algoritm to choose their action
+ `neural_player.py` implements an agent with a neural network to choose its actions
+ `human_player.py` implements an agent that allows human user to interact with an another player
+ `mcts_player` implements an agent using Monte Carlo Tree Search (MCTS) algorithm to choose action
+ `other_player.py` implement an additional agent with a specific strategy

The files `training_mcts.py`, `training_neural.py`, `training_qlearning.py`, `train_with_new_ai.py` contain code to train our agents and `play_game.py` helps to compare our agents to each other by running several games and saving the results in `games_results.txt`.

Our graphical interface is implemented with the following files:

+ `params.py` contains somes parameters
+ `board_graphics.py` contains a class `Carte` which inherits from Canvas for the grid
+ `computer_players` loads agents used to play against a human user
+ `interface` which features an agent with which a human user interacts

The remaining files, `board.py` and `board_cache.py` implement respectively a class representing the game grid for the console and a class for the memory of the agents to train.

## How to use
To experiment with these agents, please clone this repository. After, run the following commands.

+ `cd puissance-4`
+ `pip install -r requirements.txt`
+ `python interface.py`