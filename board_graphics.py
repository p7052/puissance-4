from tkinter import *

class Carte(Canvas):

    def __init__(self,parent,params,matrice,**kwargs):
        Canvas.__init__(self, parent, width=700, height=600, bg="white", bd=0, **kwargs)
        
        self.c=100
        self.b=1
        self.couleur=params.couleur
        c,b=self.c,self.b

        #Create a blue background
        [[self.create_rectangle(x*c+b,y*c+b,(x+1)*c+b,(y+1)*c+b,fill='blue') for y in range(6)] for x in range(7)]

        #Create a circle in each box (represent the tokens)
        self.rep=[[self.create_oval(x*c+b+4,y*c+b+4,(x+1)*c+b-4,(y+1)*c+b-4,fill=self.couleur[matrice[x][y]],width=b,outline='blue') for y in range(6)] for x in range(7)]

        self.pack()
        self.master.resizable(width=FALSE, height=FALSE)

    def change_parcelle(self,x,y,v):
        self.itemconfig(self.rep[x][y],fill=self.couleur[v])

    #Gives the column in which the player has clicked
    def get_x(self,event):
        x=int((event.x-self.b)/self.c)
        return x
