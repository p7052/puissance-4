from player import AbstractPlayer
from board_cache import Cache
from board import Cell


class Minimax(AbstractPlayer):
    """
        Minimax Agent player
    """

    def __init__(self):
        super().__init__("Minimax")
        self.cache = Cache()
        #self.time_taken = 0

    def min_or_max(self, board):
        """
            returns the min function if it is O’s turn and max if it is X’s turn
        """
        return min if board.whose_turn() == Cell.O else max

    def calculate_position_value(self, board):

        """
            finds the value for a given board when it is not already in the cache
            If we’re at the end of a game, we return the game result as the value for the position
            Otherwise, we recursively call back into get_position_value with each of the valid possible moves
        """

        if board.is_game_over():
            return board.get_game_result()

        moves = board.get_valid_moves()

        min_or_max = self.min_or_max(board)

        move_values = [self.get_move_value(move, board)
                       for move in moves]

        return min_or_max(move_values)


    def get_move_value(self, move, board):

        """
            gets the value for each of the next moves from the current board position
        """
        new_board = board.simulate_turn(move)
        cached, found = self.cache.get(new_board)

        if found:
            return cached

        value = self.calculate_position_value(new_board)
        self.cache.set(new_board, value)

        return value

    def get_move_value_pairs(self, board):
        
        moves = board.get_valid_moves()
        assert moves, "No valid moves"

        return [(move, self.get_move_value(move, board))
                for move in moves]

    def filter(self, board, move_value_pairs):
        """
            Apply min or max function on the list of value associated to each move
        """
        min_or_max = self.min_or_max(board)
        move, value = min_or_max(move_value_pairs, key=lambda pair: pair[1])
        return move

    def get_best_move(self, board):
        
        move_value_pairs = self.get_move_value_pairs(board)
        return self.filter(board, move_value_pairs)