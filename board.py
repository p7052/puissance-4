"""
    We will define the board class on which agents should play
"""

from enum import IntEnum
import numpy as np
import random

class Cell(IntEnum):
    """
        This is an enumeration of 3 constants that are also subclasses of int.
        A cell can be Empty or marked by one of the players. 
    """
    Empty = 0
    X = 1  # for player 1
    O = -1  # for player 2


class Result(IntEnum):
    """
        This is an enumeration of 4 constants for the game's result. 
    """
    X_Wins = 1
    O_Wins = -1
    Draw = 0
    Incomplete = 2


dim = 7 # the board dimension
nb_lines = 6
col_sample = [0, 0, 0, 0, 0, 0]


class Board(object):
    """
        This is the board with cells as attribute.
        cells is a list, if it's not None
    """

    def __init__(self, cells=None):
        
        #if cells is None, we will get an empty board.
        
        super(Board, self).__init__()
        if cells is None:
            self.cells = np.array([col_sample]*7).transpose()
        else:
            self.cells = cells.copy()

    def cells_2d(self):
        """
            Will be used for to get, row, columns and diagonals
        """
        return self.cells

    def whose_turn(self):

        # We count non empty cells to know who will play next

        non_zero_count = np.count_nonzero(self.cells)
        return Cell.X if (non_zero_count % 2 == 0) else Cell.O
    
    def get_valid_moves(self):
        return [i for i in range(dim)
                if Cell.Empty in self.cells[:, i]]

    def get_invalid_moves(self):
        return [i for i in range(dim)
                if Cell.Empty not in self.cells[:, i]]

    def get_random_valid_move(self):
        if self.get_valid_moves():
            return random.choice(self.get_valid_moves())
    
    def execute_turn(self, move):

        if move in self.get_valid_moves():

            idx = nb_lines - 1 - list(reversed(list(self.cells[:, move]))).index(Cell.Empty)
            self.cells[idx, move] = self.whose_turn()
    
    def get_symbol(self, cell):

        if cell == Cell.Empty:
            return ' '

        if cell == Cell.X:
            return 'X'

        if cell == Cell.O:
            return 'O'

        assert False, "Undefined Puissance 4 cell"

    def print(self):
        
        print('\n-----------------------------')

        for row in range(nb_lines):
            print('|', end="")

            for col in range(dim):
                cell = self.cells_2d()[row][col]
                print(" %s " % self.get_symbol(cell), end="|")

            if row < dim - 1:
                print("\n-----------------------------")

        print('\n')
    
    def is_move_valid(self, move):

        return move in self.get_valid_moves()

    def is_game_over(self):
        return self.get_game_result() != Result.Incomplete
    
    def get_rows_cols_and_diagonals(self):

        rows = [self.cells[i,:] for i in range(nb_lines)]
        cols = [self.cells[:, j] for j in range(dim)]
        diags = [np.diagonal(self.cells, offset= i) for i in [-2, -1, 0, 1, 2, 3]]
        anti_diags = [np.diagonal(np.fliplr(self.cells), offset= i) for i in [-2, -1, 0, 1, 2, 3]]
        
        return rows + cols + diags + anti_diags
    
    
    def get_depth(self):
        return sum(cell != Cell.Empty for cell in self.cells)
    
    def simulate_turn(self, move):
        """
            Simulate move for minimax , Qlearning algorithm
        """
        new_board = Board(self.cells)
        new_board.execute_turn(move)
        return new_board
    
    def get_game_result(self):
        
        rows_cols_and_diagonals = self.get_rows_cols_and_diagonals()

        sums = []
        
        for elt in rows_cols_and_diagonals:
            j = len(elt) - 3
            sums += [sum(elt[i:i+4]) for i in range(j)]
        
        max_value = max(sums)
        min_value = min(sums)

        if max_value == 4:
            return Result.X_Wins

        if min_value == -4:
            return Result.O_Wins

        if not self.get_valid_moves():
            return Result.Draw

        return Result.Incomplete