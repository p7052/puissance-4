import pickle
from torch.nn import MSELoss
from neural_agent import QNeural
from qlearning_agent import QLearning
from other_player import IA
loss = MSELoss()


ia = IA()
loss = MSELoss()

def qlearning_training(opponent, turn, epsilon, name_, double = False, explore = False, total_games= 2000):

    agent_trained = QLearning(epsilon = epsilon, use_double = double, var_explore = explore)
    agent_trained.train(turn = turn, name_file = name_, opponent = opponent, total_games= total_games)

    with open(str(agent_trained.name)+name_, 'wb') as file:
        pickle.dump(agent_trained.tables, file)

def qneural_training(opponent, turn, epsilon, name_, double = False, explore = False, total_games= 2000):

    agent_trained = QNeural(loss_function = loss, epsilon = epsilon, use_double = double, var_explore = explore)
    agent_trained.train(turn = turn, name_file = name_, opponent = opponent, total_games= total_games)
    agent_trained.save(name_)

if __name__=='__main__':

    qlearning_training(opponent = ia, turn = 1, epsilon = 0.1, name_ = "_QL_vs_ia_100000_eps01_", double = False, explore = False, total_games= 150000)
    qlearning_training(opponent = ia, turn = 2, epsilon = 0.1, name_ = "_ia_vs_QL_100000_eps01_", double = False, explore = False, total_games= 150000)

    print("\nTraining QN agent as second player with ai agent\n")
    qneural_training(opponent = ia, turn = 2, epsilon = 0.1, name_ = "_ia_vs_QN_100000_eps01_", double = False, explore = False, total_games= 150000)

    print("\nTraining QN agent as first player with ia agent\n")
    qneural_training(opponent = ia, turn = 1, epsilon = 0.1, name_ = "_QN_vs_ia_100000_eps01_", double = False, explore = False, total_games= 150000)