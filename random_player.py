from player import AbstractPlayer


class Random(AbstractPlayer):
    """
    docstring for Random
    """

    def __init__(self):
        super().__init__('Random')

    def get_best_move(self, board):
        return board.get_random_valid_move()