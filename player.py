from abc import ABC, abstractmethod

class AbstractPlayer(ABC):
    """
        Abstract class from which all players must inherit.
    """

    def __init__(self, name):
        self.turn = 0
        self.name = name
        self.wins = 0
        self.draws = 0
        self.losses = 0
        self.games = 0

    def set_turn(self, turn):
        assert turn >= 1 or turn <= 2, f"Invalid turn set: {turn}. Player is designed for 2 player games."
        self.turn = turn

    @abstractmethod
    def get_best_move(self, board):
        raise NotImplementedError

    def move(self, board):
        board.execute_turn(self.get_best_move(board))