from board import Cell, Result, Board
from qlearning_agent import QLearning
from neural_agent import QNeural
from random_player import Random
from mcts import Mcts
from other_player import IA
import pickle
from torch.nn import MSELoss
from tqdm import tqdm, trange
import time

loss = MSELoss()
PRINT = False

def play_game(x_player, o_player):
    x_player.set_turn(1)
    o_player.set_turn(2)
    board = Board()

    while not board.is_game_over():
       
        if board.whose_turn() == Cell.X:
            player = x_player
        else :
            player = o_player

        if player.name == "IA":
            board.cells = player.jouer_ia(board)
        else:
            player.move(board)

    if PRINT:
        board.print()

    if PRINT and board.is_game_over():
        print(board.get_game_result().name)

    return board


def play_games(x_player, o_player, total_games = 2000, record = True):
    results = {
        Result.X_Wins: 0,
        Result.O_Wins: 0,
        Result.Draw: 0
    }

    print("%s as X and %s as O" % (x_player.name, o_player.name), flush=True)
    print("Playing %d games" % total_games, flush=True)

    time.sleep(0.05) # Ensures no collisions between tqdm prints and main prints
    for _ in trange(total_games):
        end_of_game = play_game(x_player, o_player)
        result = end_of_game.get_game_result()
        results[result] += 1

    
    x_wins_percent = results[Result.X_Wins] / total_games * 100
    o_wins_percent = results[Result.O_Wins] / total_games * 100
    draw_percent = results[Result.Draw] / total_games * 100

    if record:

        with open("games_result.txt", "a") as f:

            f.write("\n" + f"{x_player.name} vs {o_player.name}" +"\n")
            f.write("\n")
            f.write(f"x wins: {x_wins_percent:.2f}%"+"\n")
            f.write(f"o wins: {o_wins_percent:.2f}%"+"\n")
            f.write(f"draw  : {draw_percent:.2f}%"+"\n")

    else : 
        print(f"x wins: {x_wins_percent:.2f}%")
        print(f"o wins: {o_wins_percent:.2f}%")
        print(f"draw  : {draw_percent:.2f}%")
        print("")

x_players = [Random()]

with open("./Trained agent/QLearning_QL_vs_R_100000_eps01_", "rb") as input:
    x_QT = QLearning(epsilon = 0.1, var_explore= False, playing=True)
    x_QT.set_tables(pickle.Unpickler(input).load())
    x_players.append(x_QT)

with open("./Trained agent/Double QLearning_QL_vs_R_100000_eps01_DQN_", "rb") as input:
    x_DQT = QLearning(epsilon = 0.1, use_double = True, var_explore= False, playing=True)
    x_DQT.set_tables(pickle.Unpickler(input).load())
    x_players.append(x_DQT)

x_QN = QNeural(loss_function= loss, var_explore= False, epsilon= 0.1, playing=True)
x_QN.load("./neural_checkpoints/checkpoint_QN_vs_R_100000_eps01__1646184445_100000")
x_players.append(x_QN)

x_DQN = QNeural(loss_function= loss, use_double = True, var_explore= False, epsilon= 0.1, playing=True)
x_DQN.load("./neural_checkpoints/checkpoint_QN_vs_R_100000_eps01_DQN__1646198061_100000")
x_players.append(x_DQN)

with open("./Trained agent/Monte Carlo Tree Search_nodes", 'rb') as input:
    mcts = Mcts()
    mcts.set_nodes(pickle.Unpickler(input).load())

x_players.append(mcts)

o_players = [Random()]

with open("./Trained agent/QLearning_R_vs_QL_100000_eps01_", "rb") as input:
    o_QT = QLearning(epsilon = 0.1, var_explore= False, playing=True)
    o_QT.set_tables(pickle.Unpickler(input).load())
    o_players.append(o_QT)

with open("./Trained agent/Double QLearning_R_vs_QL_100000_eps01_DQN_", "rb") as input:
    o_DQT = QLearning(epsilon = 0.1, use_double = True, var_explore= False, playing=True)
    o_DQT.set_tables(pickle.Unpickler(input).load())
    o_players.append(o_DQT)

o_QN = QNeural(loss_function= loss, var_explore= False, epsilon= 0.1, playing=True)
o_QN.load("./neural_checkpoints/checkpoint_R_vs_QN_100000_eps01__1646212660_100000")
o_players.append(o_QN)

o_DQN = QNeural(loss_function= loss, use_double = True, var_explore= False, epsilon= 0.1, playing=True)
o_DQN.load("./neural_checkpoints/checkpoint_R_vs_QN_100000_eps01_DQN__1646213903_100000")
o_players.append(o_DQN)

o_players.append(mcts)

ia = IA()

with open("./Trained agent/QLearning_QL_vs_ia_100000_eps01_", "rb") as input:
    x_QT_ia = QLearning(epsilon = 0.1, var_explore= False)
    x_QT_ia.set_tables(pickle.Unpickler(input).load())

with open("./Trained agent/QLearning_ia_vs_QL_100000_eps01_", "rb") as input:
    o_QT_ia = QLearning(epsilon = 0.1, var_explore= False)
    o_QT_ia.set_tables(pickle.Unpickler(input).load())

x_QN_ia = QNeural(loss_function= loss, var_explore= False, epsilon= 0.1)
x_QN_ia.load("./neural_checkpoints/checkpoint_QN_vs_ia_100000_eps01__1646511176_150000")

o_QN_ia = QNeural(loss_function= loss, var_explore= False, epsilon= 0.1)
o_QN_ia.load("./neural_checkpoints/checkpoint_ia_vs_QN_100000_eps01__1646506712_150000")

if __name__=='__main__':

    for x_player in x_players:

        for o_player in o_players:

            play_games(x_player, o_player)
    
    with open("games_result.txt", "a") as f:
        f.write("\n##########################################\n################################################\n")

    for x_player in x_players:
        play_games(x_player, ia)
        play_games(x_player, o_QT_ia)
        play_games(x_player, o_QN_ia)

    for o_player in o_players:
        play_games(ia, o_player)
        play_games(x_QT_ia, o_player)
        play_games(x_QN_ia, o_player)

    play_games(ia, o_QT_ia)
    play_games(ia, o_QN_ia)
    play_games(x_QT_ia, ia)
    play_games(x_QN_ia, ia)
    play_games(x_QN_ia, o_QN_ia)
    play_games(x_QT_ia, o_QT_ia)
    