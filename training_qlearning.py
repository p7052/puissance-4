import pickle
from torch.nn import MSELoss
from qlearning_agent import QLearning
from random_player import Random
from minimax_player import Minimax

random_ = Random()
minimax = Minimax()
loss = MSELoss()

def qlearning_training(opponent, turn, epsilon, name_, double = False, explore = False, total_games= 2000):

    agent_trained = QLearning(epsilon = epsilon, use_double = double, var_explore = explore)
    agent_trained.train(turn = turn, name_file = name_, opponent = opponent, total_games= total_games)

    with open(str(agent_trained.name)+name_, 'wb') as file:
        pickle.dump(agent_trained.tables, file)

if __name__=='__main__':

    #print("\nTraining QL agent as first player with Random agent\n")
    #qlearning_training(opponent = random_, turn = 1, epsilon = 0.3, name_ = "_QL_vs_R_100000_eps03_", double = False, explore = False, total_games= 100000)

    #print("\nTraining QL agent as first player with Random agent\n")
    #qlearning_training(opponent = random_, turn = 1, epsilon = 0.2, name_ = "_QL_vs_R_100000_eps02_", double = False, explore = False, total_games= 100000)

    #print("\nTraining QL agent as first player with Random agent\n")
    #qlearning_training(opponent = random_, turn = 1, epsilon = 0.1, name_ = "_QL_vs_R_100000_eps01_", double = False, explore = False, total_games= 100000)

    print("\nTraining QL agent as first player with Random agent\n")
    qlearning_training(opponent = random_, turn = 1, epsilon = 0.5, name_ = "_QL_vs_R_100000_", double = False, explore = True, total_games= 100000)

    #print("\nTraining QL agent as first player with Random agent\n")
    #qlearning_training(opponent = random_, turn = 1, epsilon = 0.3, name_ = "_QL_vs_R_100000_eps03_DQN_", double = True, explore = False, total_games= 100000)

    #print("\nTraining QL agent as first player with Random agent\n")
    #qlearning_training(opponent = random_, turn = 1, epsilon = 0.2, name_ = "_QL_vs_R_100000_eps02_DQN_", double = True, explore = False, total_games= 100000)

    #print("\nTraining QL agent as first player with Random agent\n")
    #qlearning_training(opponent = random_, turn = 1, epsilon = 0.1, name_ = "_QL_vs_R_100000_eps01_DQN_", double = True, explore = False, total_games= 100000)
    
    print("\nTraining QL agent as first player with Random agent\n")
    qlearning_training(opponent = random_, turn = 1, epsilon = 0.5, name_ = "_QL_vs_R_100000_DQN_", double = True, explore = True, total_games= 100000)

    print("\nTraining QL agent as first player with Random agent\n")
    qlearning_training(opponent = random_, turn = 2, epsilon = 0.1, name_ = "_R_vs_QL_100000_eps01_", double = False, explore = False, total_games= 100000)

    print("\nTraining QL agent as first player with Random agent\n")
    qlearning_training(opponent = random_, turn = 2, epsilon = 0.1, name_ = "_R_vs_QL_100000_eps01_DNQ_", double = True, explore = False, total_games= 100000)