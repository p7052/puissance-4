from mcts import Mcts
import pickle
from training_qlearning import qlearning_training
from training_neural import qneural_training
import time
if __name__=='__main__':
    
    A = Mcts()
    A.train(playouts=50000)
    
    with open("Monte Carlo Tree Search_nodes", 'wb') as file:
        pickle.dump(A.nodes, file)
    
    t = time.time()
    with open("Monte Carlo Tree Search_nodes", 'rb') as file:
        A.set_nodes(pickle.Unpickler(file).load())
    
    print(time.time() - t)
    print("\nTraining QL agent as first player with MTCS agent\n")
    qlearning_training(opponent = A, turn = 1, epsilon = 0.1, name_ = "_QL_vs_mcts_100000_eps01_", double = False, explore = False, total_games= 10000)
    
    print("\nTraining QN agent as first player with MTCS agent\n")
    qneural_training(opponent = A, turn = 1, epsilon = 0.1, name_ = "_QN_vs_mcts_100000_eps01_", double = False, explore = False, total_games= 10000)